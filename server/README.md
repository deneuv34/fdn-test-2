## Installation

```bash
# installing project dependencies
$ npm install
```

### Database Seeding

First, you need to create database named `fdntest2`.

```bash
# seed database
$ mysql -u root -p fdntest2 < fdntest2.sql
```

## Database Configuration

Database settings are stored in `ormconfig.json` you can change it into your database setting.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```
