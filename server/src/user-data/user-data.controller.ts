import { Controller, Get, Body } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user-data')
export class UserDataController {
    constructor(
        private readonly userDataService: UserService,
    ) {}

    @Get()
    async getAll() {
        return await this.userDataService.getAll();
    }
}
