import { Test } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { UserDataController } from './user-data.controller';
import { UserService } from './user.service';

describe('UserController', () => {
    let userController: UserDataController;
    let userService: UserService;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [],
            providers: [
                UserService,
                {
                    provide: 'UserService',
                    useClass: Repository,
                },
            ],
            controllers: [UserDataController],
        }).compile();

        userController = module.get<UserDataController>(UserDataController);
        userService = module.get<UserService>(UserService);
    });

    describe('getUsers', () => {
        it('should return an array of user', async () => {
            const result = [];
            jest.spyOn(userController, 'getAll').mockImplementation(async () => result);

            expect(await userController.getAll()).toBe(result);
        });
    });
});
