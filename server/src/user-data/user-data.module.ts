import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from './user-data.entity';
import { UserService } from './user.service';
import { UserDataController } from './user-data.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Users])],
    providers: [UserService],
    controllers: [UserDataController],
})
export class UserDataModule {}
