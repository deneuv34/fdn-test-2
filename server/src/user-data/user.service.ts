import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './user-data.entity';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(Users)
        private readonly repository: Repository<Users>,
    ) {}

    public async getAll(): Promise<Users[]> {
        try {
            return await this.repository.createQueryBuilder('users')
                .select([
                    `concat(users.firstName, ' ',users.lastName) as fullName`,
                    'email',
                    'item',
                    'quantity',
                    'totalPrice',
                ])
                .getRawMany();
        } catch (error) {
            throw new HttpException(error.message, 500);
        }
    }
}
