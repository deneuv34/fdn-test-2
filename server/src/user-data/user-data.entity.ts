import { Entity, PrimaryColumn, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Users {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar')
    firstName: string;

    @Column('varchar')
    lastName: string;

    @Column('varchar')
    email: string;

    @Column('varchar')
    item: string;

    @Column('int')
    quantity: number;

    @Column('decimal', {precision: 18, scale: 2})
    totalPrice: number;
}
