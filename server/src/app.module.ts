import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserDataModule } from './user-data/user-data.module';

@Module({
  imports: [TypeOrmModule.forRoot(), UserDataModule],
})
export class AppModule {}
