import { mount } from '@vue/test-utils'
import App from '../../src/App.vue'

const wrapper = mount(App)

describe('Dashboard', () => {
	it('matches snapshot', () => {
		expect(wrapper.html()).toMatchSnapshot()
	})
});
