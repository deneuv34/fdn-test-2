# frontend

## Project setup
```bash
$ npm install
```

### Compiles and hot-reloads for development
```bash
# web will listen on localhost:8080
$ npm run serve
```

### Compiles and minifies for production
```bash
$ npm run build
```

### Run your tests
```bash
$ npm run test
```

### Lints and fixes files
```bash
$ npm run lint
```