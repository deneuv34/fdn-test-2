# Female Daily Network Test - 2 (Data Pivot)

This repository is for Female Daily Network test assessment.
- Server-side data seed / database backup are inside the `server` directory
- All project inside are using GitLab CI/CD for test automation
- Every CI configuration are written on `.gitlab-ci.yml` inside projects directory

## How to Run it

### Run Server

```bash
$ cd server
```

And follow The instruction [here](https://gitlab.com/deneuv34/fdn-test-2/blob/master/server/README.md).

### Run Client

```bash
$ cd frontend
```

And follow the instruction [here](https://gitlab.com/deneuv34/fdn-test-2/blob/master/frontend/README.md). Web will running & listen on: http://localhost:8080

## Todo
- *Nothing*

## Author
Rangga Adhitya Prawira (deneuv3.4@gmail.com)

*Git Profile*: [GitHub](https://github.com/deneuv34), [GitLab](https://github.com/deneuv34)